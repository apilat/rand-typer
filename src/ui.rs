use anyhow::{anyhow, Context};
use crossterm::cursor::{Hide, MoveTo, Show};
use crossterm::style::{
    Attribute, Color, Print, SetAttribute, SetBackgroundColor, SetForegroundColor,
};
use crossterm::terminal::{
    self, disable_raw_mode, enable_raw_mode, Clear, ClearType, EnterAlternateScreen,
    LeaveAlternateScreen,
};
use crossterm::{ExecutableCommand, QueueableCommand};
use std::cmp::Ordering;
use std::io::Write;
use std::ops::Add;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Tile {
    c: char,
    fg: Color,
    bg: Color,
    style: Attribute,
}

impl Tile {
    pub fn char(self, c: char) -> Self {
        Tile { c, ..self }
    }

    pub fn fg(self, fg: Color) -> Self {
        Tile { fg, ..self }
    }

    pub fn bg(self, bg: Color) -> Self {
        Tile { bg, ..self }
    }

    pub fn style(self, style: Attribute) -> Self {
        Tile { style, ..self }
    }

    fn write(&self, output: &mut dyn Write) -> crossterm::Result<()> {
        output
            .queue(SetAttribute(self.style))?
            .queue(SetForegroundColor(self.fg))?
            .queue(SetBackgroundColor(self.bg))?
            .queue(Print(self.c))?;
        Ok(())
    }
}

impl Default for Tile {
    fn default() -> Self {
        Tile {
            c: ' ',
            bg: Color::Reset,
            fg: Color::Reset,
            style: Attribute::Reset,
        }
    }
}

#[derive(Debug, Clone)]
struct Screen {
    width: u16,
    height: u16,
    data: Vec<Tile>,
}

impl Screen {
    fn empty(width: u16, height: u16) -> Self {
        Screen {
            width,
            height,
            data: vec![Tile::default(); width as usize * height as usize],
        }
    }

    fn write(&self, old: &mut Screen, output: &mut dyn Write) -> anyhow::Result<()> {
        let equal_size = old.width == self.width && old.height == self.height;
        if !equal_size {
            output.queue(Clear(ClearType::All)).context("clear")?;
            *old = Screen::empty(self.width, self.height);
        }

        for x in 0..self.width {
            for y in 0..self.height {
                let c = self.char_at(x, y);
                if c != old.char_at(x, y) {
                    output.queue(MoveTo(x, y)).context("cursor move")?;
                    c.write(output)
                        .with_context(|| format!("write character {},{}", x, y))?;
                    *old.char_at_mut(x, y) = c;
                }
            }
        }

        output.flush().context("flush")?;
        Ok(())
    }

    fn char_at(&self, x: u16, y: u16) -> Tile {
        self.data[y as usize * self.width as usize + x as usize]
    }

    fn char_at_mut(&mut self, x: u16, y: u16) -> &mut Tile {
        &mut self.data[y as usize * self.width as usize + x as usize]
    }

    fn set_tile(&mut self, x: u16, y: u16, t: Tile) {
        *self.char_at_mut(x, y) = t;
    }
}

pub struct Backend {
    buffer: Screen,
    output: Box<dyn Write>,
}

impl Backend {
    pub fn new<W: Write + 'static>(mut output: W) -> anyhow::Result<Self> {
        output
            .execute(EnterAlternateScreen)
            .context("enter alternate screen")?;
        output.execute(Hide).context("hide cursor")?;
        enable_raw_mode().context("enable raw mode")?;
        Ok(Backend {
            output: Box::new(output),
            buffer: Screen::empty(0, 0),
        })
    }

    pub fn finish(mut self) -> anyhow::Result<()> {
        disable_raw_mode().context("disable raw mode")?;
        self.output.execute(Show).context("show cursor")?;
        self.output
            .execute(LeaveAlternateScreen)
            .context("leave alternate screen")?;
        Ok(())
    }

    pub fn begin_draw(&mut self) -> anyhow::Result<DrawContext> {
        DrawContext::new(self)
    }
}

impl Drop for Backend {
    fn drop(&mut self) {
        // Cannot do anything about errors now, so just ignore
        let _ = disable_raw_mode();
        let _ = self.output.execute(Show);
        let _ = self.output.execute(LeaveAlternateScreen);
    }
}

pub struct DrawContext<'a> {
    backend: &'a mut Backend,
    offset: (u16, u16),
    size: (u16, u16),
    buffer: Screen,
}

impl<'a> DrawContext<'a> {
    pub fn new(backend: &'a mut Backend) -> anyhow::Result<Self> {
        let size = terminal::size().context("terminal size")?;
        Ok(DrawContext {
            backend,
            offset: (0, 0),
            size,
            buffer: Screen::empty(size.0, size.1),
        })
    }

    pub fn end_draw(self) -> anyhow::Result<()> {
        self.buffer
            .write(&mut self.backend.buffer, &mut self.backend.output)?;
        Ok(())
    }

    pub fn draw<W: Widget>(&mut self, widget: &W) -> anyhow::Result<()> {
        widget.draw_to(self)
    }

    pub fn size(&self) -> (u16, u16) {
        self.size
    }

    pub fn width(&self) -> u16 {
        self.size.0
    }

    pub fn height(&self) -> u16 {
        self.size.1
    }

    pub fn print_str(&mut self, s: &str) -> anyhow::Result<()> {
        self.print_str_styled(s, Tile::default())
    }

    // TODO Better API for styles
    pub fn print_str_styled(&mut self, s: &str, style: Tile) -> anyhow::Result<()> {
        if s.len() > self.width() as usize {
            return Err(anyhow!(
                "string of length {} too long for context of width {}",
                s.len(),
                self.width()
            ));
        }
        if self.height() == 0 {
            return Err(anyhow!("cannot print string in zero-height context"));
        }

        for (i, c) in s.chars().enumerate() {
            self.buffer
                .set_tile(self.offset.0 + i as u16, self.offset.1, style.char(c));
        }
        Ok(())
    }

    pub fn print_char(&mut self, c: char) -> anyhow::Result<()> {
        self.print_tile(Tile::default().char(c))
    }

    pub fn print_tile(&mut self, t: Tile) -> anyhow::Result<()> {
        if self.width() == 0 {
            return Err(anyhow!("cannot print char in zero-width context"));
        }
        if self.height() == 0 {
            return Err(anyhow!("cannot print char in zero-height context"));
        }

        self.buffer.set_tile(self.offset.0, self.offset.1, t);
        Ok(())
    }

    pub fn with_offset(
        &mut self,
        (left, top): (u16, u16),
        f: impl FnOnce(&mut DrawContext) -> anyhow::Result<()>,
    ) -> anyhow::Result<()> {
        let (width, height) = self.size();
        if left > width {
            return Err(anyhow!("left offset {} larger than width {}", left, width));
        }
        if top > height {
            return Err(anyhow!("top offset {} larger than height {}", top, height));
        }

        let old_offset = self.offset;
        let old_size = self.size;
        self.offset = (self.offset.0 + left, self.offset.1 + top);
        self.size = (width - left, height - top);
        let result = f(self);
        self.offset = old_offset;
        self.size = old_size;
        result
    }
}

#[derive(PartialEq, Eq)]
pub enum Size {
    Exact(u16),
    Fill,
}

impl PartialOrd for Size {
    fn partial_cmp(&self, other: &Size) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Size {
    fn cmp(&self, other: &Size) -> Ordering {
        use Size::*;
        match (self, other) {
            (Exact(a), Exact(b)) => a.cmp(b),
            (Exact(_), Fill) => Ordering::Less,
            (Fill, Exact(_)) => Ordering::Greater,
            (Fill, Fill) => Ordering::Equal,
        }
    }
}

impl Add for Size {
    type Output = Size;
    fn add(self, rhs: Size) -> Size {
        use Size::*;
        match (self, rhs) {
            (Exact(a), Exact(b)) => Exact(a.max(b)),
            (Exact(_), Fill) | (Fill, Exact(_)) | (Fill, Fill) => Fill,
        }
    }
}

pub trait Widget {
    // TODO Extend this to include bounds like min/max
    fn size(&self) -> (Size, Size);
    fn draw_to(&self, ctx: &mut DrawContext) -> anyhow::Result<()>;
}

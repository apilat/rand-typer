use crate::ui::{DrawContext, Size, Tile, Widget};
use anyhow::anyhow;
use crossterm::style::Attribute;
use std::convert::TryInto;
use std::ops::Add;

#[derive(Debug, Clone)]
pub struct Text<'a> {
    text: &'a str,
}

impl<'a> Text<'a> {
    pub fn new(text: &'a str) -> anyhow::Result<Self> {
        if TryInto::<u16>::try_into(text.len()).is_ok() {
            Ok(Text { text })
        } else {
            Err(anyhow!("text length {} exceeds limit", text.len()))
        }
    }
}

impl Widget for Text<'_> {
    fn size(&self) -> (Size, Size) {
        (Size::Exact(self.text.len() as u16), Size::Exact(1))
    }

    fn draw_to(&self, ctx: &mut DrawContext) -> anyhow::Result<()> {
        ctx.print_str(self.text)
    }
}

#[derive(Debug, Clone)]
pub struct Center<'a, W: Widget> {
    widget: &'a W,
}

impl<'a, W: Widget> Center<'a, W> {
    pub fn new(widget: &'a W) -> Self {
        Center { widget }
    }
}

impl<W: Widget> Widget for Center<'_, W> {
    fn size(&self) -> (Size, Size) {
        (Size::Fill, Size::Fill)
    }

    fn draw_to(&self, ctx: &mut DrawContext) -> anyhow::Result<()> {
        // Don't deal with overflows here, but instead delegate it to DrawContext
        let (width, height) = self.widget.size();
        let left = match width {
            Size::Exact(width) => (ctx.width().saturating_sub(width)) / 2,
            Size::Fill => 0,
        };
        let top = match height {
            Size::Exact(height) => (ctx.height().saturating_sub(height)) / 2,
            Size::Fill => 0,
        };
        ctx.with_offset((left, top), |ctx| ctx.draw(self.widget))?;
        Ok(())
    }
}

pub struct Column<'a> {
    children: &'a [&'a dyn Widget],
}

impl<'a> Column<'a> {
    pub fn new(children: &'a [&'a dyn Widget]) -> Self {
        Column { children }
    }
}

impl Widget for Column<'_> {
    fn size(&self) -> (Size, Size) {
        let mut size = (Size::Exact(0), Size::Exact(0));
        for child in self.children {
            let (width, height) = child.size();
            size = (size.0.max(width), size.1.add(height));
        }
        size
    }

    fn draw_to(&self, ctx: &mut DrawContext) -> anyhow::Result<()> {
        let mut offset = 0;
        for child in self.children {
            let (_, height) = child.size();
            let height = match height {
                Size::Exact(x) => x,
                Size::Fill => return Err(anyhow!("column component fills height")),
            };
            ctx.with_offset((0, offset), |ctx| child.draw_to(ctx))?;
            offset += height;
        }
        Ok(())
    }
}

pub struct Selection<'a> {
    lines: &'a [&'a str],
    selected: usize,
}

impl<'a> Selection<'a> {
    pub fn new(lines: &'a [&'a str], selected: usize) -> anyhow::Result<Self> {
        if selected >= lines.len() {
            // This also deals with the case of lines.len() == 0
            Err(anyhow!(
                "selected index {} exceeds option count {}",
                selected,
                lines.len()
            ))
        } else if let Some((i, x)) = lines
            .iter()
            .enumerate()
            .find(|(_, x)| TryInto::<u16>::try_into(x.len()).is_err())
        {
            Err(anyhow!(
                "option {} line length {} exceeds u16 limit",
                i,
                x.len()
            ))
        } else {
            Ok(Selection { lines, selected })
        }
    }
}

impl Widget for Selection<'_> {
    fn size(&self) -> (Size, Size) {
        (
            Size::Exact(self.lines.iter().map(|x| x.len()).max().unwrap() as u16),
            Size::Exact(self.lines.len() as u16),
        )
    }

    fn draw_to(&self, ctx: &mut DrawContext) -> anyhow::Result<()> {
        for (i, text) in self.lines.iter().enumerate() {
            ctx.with_offset((0, i as u16), |ctx| {
                ctx.print_str_styled(
                    text,
                    if i == self.selected {
                        Tile::default().style(Attribute::Reverse)
                    } else {
                        Tile::default()
                    },
                )
            })?;
        }
        Ok(())
    }
}

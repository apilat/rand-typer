use super::Chain;
use crate::word_source::WordSource;
use anyhow::Context;
use once_cell::sync::Lazy;
use rand::prelude::StdRng;
use regex::Regex;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Token {
    Word(String),
    Punctuation(char),
    End(char),
}

impl<'a, const N: usize> Chain<Token, N> {
    fn extract_project_gutenberg_text(text: &str) -> anyhow::Result<&str> {
        let start = (|| {
            static START_REGEX: Lazy<Regex> = Lazy::new(|| {
                Regex::new(r"\*\*\* ?START OF TH(?:(?:IS)|E) PROJECT GUTENBERG EBOOK").unwrap()
            });
            let end_text = "***";
            let start = START_REGEX.find(&text)?.end();
            let end = text[start..].find(end_text)?;
            Some(start + end + end_text.len())
        })()
        .context("start not found")?;

        let end = {
            static END_REGEX: Lazy<Regex> = Lazy::new(|| {
                Regex::new(r"\*\*\* ?END OF TH(?:(?:IS)|E) PROJECT GUTENBERG EBOOK").unwrap()
            });
            start
                + END_REGEX
                    .find(&text[start..])
                    .context("end not found")?
                    .start()
        };

        Ok(&text[start..end])
    }

    pub fn from_project_gutenberg(texts: Vec<String>) -> anyhow::Result<Self> {
        #[derive(Debug)]
        struct Tokenizer<'a> {
            s: &'a str,
        }
        impl<'a> Tokenizer<'a> {
            fn new(s: &'a str) -> Self {
                Tokenizer { s }
            }
        }
        impl<'a> Iterator for Tokenizer<'a> {
            type Item = Token;
            fn next(&mut self) -> Option<Self::Item> {
                let mut it = self.s.char_indices();

                while let Some((i, c)) = it.next() {
                    if c.is_whitespace() {
                        continue;
                    } else if ['.', '!', '?'].contains(&c) {
                        self.s = &self.s[i + 1..];
                        return Some(Token::End(c));
                    } else if [','].contains(&c) {
                        self.s = &self.s[i + 1..];
                        return Some(Token::Punctuation(c));
                    } else if !c.is_alphanumeric() {
                        continue;
                    } else {
                        let start = i;
                        let end = it
                            .find(|&(_, c)| !c.is_alphanumeric() && c != '\'' && c != '-')
                            .map(|(i, _)| i)
                            .unwrap_or(self.s.len());
                        let tok = &self.s[start..end];
                        self.s = &self.s[end..];
                        return Some(Token::Word(tok.to_owned()));
                    }
                }

                None
            }
        }

        let mut sentences = Vec::new();
        for text in texts {
            let mut sentence = Vec::new();
            // TODO Don't traverse the string twice (once in extract_.. and once in Tokenizer)
            for token in Tokenizer::new(Self::extract_project_gutenberg_text(&text)?) {
                let is_end = matches!(token, Token::End(_));
                sentence.push(token);
                if is_end {
                    sentences.push(std::mem::take(&mut sentence));
                }
            }
            sentences.push(sentence);
        }
        Ok(Chain::from_sentences(sentences))
    }
}

impl<const N: usize> WordSource for Chain<Token, N> {
    fn generate_text(&self, words: usize, rng: &mut StdRng) -> String {
        let mut s = String::new();
        let mut gen = self.generator(rng);

        for w in 0..words {
            match gen.next().unwrap() {
                Token::Word(word) => {
                    assert!(!word.is_empty(), "Markov chain returned empty word");
                    if w > 0 {
                        s.push(' ');
                    }
                    s.push_str(word);
                }

                Token::Punctuation(c) | Token::End(c) => {
                    s.push(*c);
                }
            }
        }

        s
    }
}

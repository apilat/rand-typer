use rand::Rng;
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::hash::Hash;
pub mod text;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Chain<G, const N: usize> {
    words_fwd: Vec<G>,
    map: HashMap<Ngram<N>, NextProbability>,
}

impl<G, const N: usize> Chain<G, N>
where
    G: Eq + Hash,
{
    pub fn from_sentences<R>(sentences: Vec<Vec<R>>) -> Self
    where
        G: Borrow<R>,
        R: Eq + Hash + ToOwned<Owned = G>,
    {
        let mut words_fwd: Vec<G> = Vec::new();
        let mut words_rev: HashMap<G, u32> = HashMap::new();
        let mut map: HashMap<Ngram<N>, NextProbability> = HashMap::new();

        for sentence in sentences.iter() {
            let mut state = Ngram::new();

            for word in sentence.iter() {
                let id = if let Some(id) = words_rev.get(word) {
                    *id
                } else {
                    let id: u32 = words_fwd
                        .len()
                        .try_into()
                        .expect("number of distinct words exceeds maximum");
                    words_fwd.push(R::to_owned(word));
                    words_rev.insert(R::to_owned(word), id);
                    id
                };

                // Even though it clones, this turns out to be cheaper than several lookups in the
                // hashmap with contains/insert.
                // or_insert_with also causes a measurable slowdown.
                map.entry(state.clone())
                    .or_insert(NextProbability::new())
                    .add(id);
                state.push(id);
            }

            map.entry(state).or_insert(NextProbability::new()).add_end();
        }

        Chain { words_fwd, map }
    }

    pub fn generator<'a, 'b, R: Rng>(&'a self, rng: &'b mut R) -> Generator<'a, 'b, G, R, N> {
        Generator {
            chain: &self,
            state: Ngram::new(),
            rng,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct NextProbability {
    total_count: u32,
    // Even though this requires a linear search to add a new item, it turns out (for the sample sizes
    // used here) to be faster than using a HashMap.
    // counts[i] = {id, count}
    counts: SmallVec<[(u32, u32); 4]>,
    // end_count: u32  is implied to be total_count - sum(counts.1)
}

impl NextProbability {
    pub fn new() -> Self {
        NextProbability {
            total_count: 0,
            counts: SmallVec::new(),
        }
    }

    pub fn add(&mut self, id: u32) {
        self.total_count += 1;
        if let Some((_, v)) = self.counts.iter_mut().find(|(i, _)| *i == id) {
            *v += 1;
        } else {
            self.counts.push((id, 1));
        }
    }

    pub fn add_end(&mut self) {
        self.total_count += 1;
    }

    pub fn random_next<R: Rng>(&self, rng: &mut R) -> Option<u32> {
        let selected = rng.gen_range(0..self.total_count);

        let mut run_total = 0;
        for &(id, count) in &self.counts {
            run_total += count;
            if run_total > selected {
                return Some(id);
            }
        }

        None
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
struct Ngram<const N: usize> {
    #[serde(with = "serde_arrays")]
    // usize::MAX is used as sentinel to show uninitialized fields
    grams: [u32; N],
    index: u8,
}

impl<const N: usize> Ngram<N> {
    fn new() -> Self {
        assert!(N <= u8::MAX as usize, "Ngram size exceeds maximum");
        Ngram {
            grams: [u32::MAX; N],
            index: 0,
        }
    }

    fn push(&mut self, id: u32) {
        assert!(id != u32::MAX, "gram id {} is too high", id);
        self.grams[self.index as usize] = id;
        self.index = (self.index + 1) % N as u8;
    }
}

#[derive(Debug)]
pub struct Generator<'a, 'b, G, R: Rng, const N: usize> {
    chain: &'a Chain<G, N>,
    state: Ngram<N>,
    rng: &'b mut R,
}

impl<'a, 'b, G, R, const N: usize> Iterator for Generator<'a, 'b, G, R, N>
where
    R: Rng,
{
    type Item = &'a G;
    fn next(&mut self) -> Option<Self::Item> {
        // TODO Correctly return None if chain is empty instead of panicking.
        let id = match self
            .chain
            .map
            .get(&self.state)
            .unwrap()
            .random_next(self.rng)
        {
            Some(id) => id,
            None => {
                self.state = Ngram::new();
                self.chain
                    .map
                    .get(&self.state)
                    .unwrap()
                    .random_next(self.rng)
                    .unwrap()
            }
        };

        self.state.push(id);
        Some(&self.chain.words_fwd[usize::try_from(id).unwrap()])
    }
}

mod serde_arrays {
    use serde::de::{Deserialize, Deserializer, Error, SeqAccess, Visitor};
    use serde::ser::{Serialize, SerializeSeq, Serializer};
    use std::fmt;
    use std::marker::PhantomData;

    pub fn serialize<T, S, const N: usize>(arr: &[T; N], ser: S) -> Result<S::Ok, S::Error>
    where
        T: Serialize,
        S: Serializer,
    {
        let mut seq = ser.serialize_seq(Some(N))?;
        for val in arr {
            seq.serialize_element(val)?;
        }
        seq.end()
    }

    // TODO Figure out how to initialize const-generic array with Default to avoid Copy bound.
    pub fn deserialize<'de, T, D, const N: usize>(de: D) -> Result<[T; N], D::Error>
    where
        T: Deserialize<'de> + Default + Copy,
        D: Deserializer<'de>,
    {
        struct ArrVisitor<T, const N: usize>(PhantomData<T>);
        impl<'de, T, const N: usize> Visitor<'de> for ArrVisitor<T, N>
        where
            T: Deserialize<'de> + Default + Copy,
        {
            type Value = [T; N];

            fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
                write!(f, "a sequence of exactly {} elements", N)
            }

            fn visit_seq<A: SeqAccess<'de>>(self, mut seq: A) -> Result<Self::Value, A::Error> {
                let mut arr: [T; N] = [T::default(); N];
                for i in 0..N {
                    arr[i] = seq
                        .next_element()?
                        .ok_or_else(|| A::Error::invalid_length(i, &self))?;
                }
                match seq.next_element::<T>()? {
                    None => Ok(arr),
                    Some(_) => Err(A::Error::invalid_length(N + 1, &self)),
                }
            }
        }

        de.deserialize_seq(ArrVisitor(PhantomData))
    }
}

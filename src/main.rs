use anyhow::Context;
use crossterm::event::{self, Event, KeyCode, KeyEvent};
use loader::SourceLoader;
use rand::prelude::StdRng;
use rand::SeedableRng;
use std::io::{stdout, BufWriter};
use std::time::Duration;
use type_box::{Stats, TypeBox};
use ui::Backend;
use widget::{Center, Column, Selection, Text};
mod loader;
mod markov;
mod type_box;
mod ui;
mod widget;
mod word_source;

#[derive(Debug)]
enum State {
    Intro { selected: usize },
    LoadingGenerator(usize),
    Typing(TypeBox),
    Stats(Stats),
}

impl State {
    fn draw(&self, frame: &mut Backend, data: &Global) -> anyhow::Result<()> {
        let mut ctx = frame.begin_draw().context("begin draw")?;

        match self {
            State::Intro { selected } => {
                let names: Vec<&str> = data.sources.names().collect();
                ctx.draw(&Center::new(&Selection::new(&names, *selected)?))?;
            }

            State::Typing(type_box) => {
                ctx.draw(&Center::new(&type_box))?;
            }

            State::Stats(stats) => {
                let accuracy_text = format!("Accuracy: {:.1}%", stats.accuracy * 100.0);
                let wpm_text = format!("WPM: {:.1}", stats.wpm);
                ctx.draw(&Center::new(&Column::new(&[
                    &Text::new(&accuracy_text)?,
                    &Text::new(&wpm_text)?,
                ])))?;
            }

            State::LoadingGenerator(_) => {
                ctx.draw(&Center::new(&Text::new("Loading generator...")?))?;
            }
        }

        ctx.end_draw().context("end draw")?;
        Ok(())
    }

    fn update(&mut self, data: &mut Global) -> anyhow::Result<()> {
        match self {
            State::LoadingGenerator(id) => {
                if let Some(generator) = data.sources.get(*id) {
                    const DEFAULT_WORD_COUNT: usize = 50;
                    *self = State::Typing(TypeBox::new(
                        generator
                            .context("word source")?
                            .generate_text(DEFAULT_WORD_COUNT, &mut data.rng),
                    )?);
                }
                Ok(())
            }

            _ => Ok(()),
        }
    }

    fn handle_event(&mut self, event: KeyEvent, data: &mut Global) -> anyhow::Result<()> {
        match self {
            State::Intro { selected } => match event.code {
                // TODO Move this handling to Selection widget
                KeyCode::Char('k') | KeyCode::Up => {
                    *self = State::Intro {
                        selected: (*selected + data.sources.len() - 1) % data.sources.len(),
                    };
                }
                KeyCode::Char('j') | KeyCode::Down => {
                    *self = State::Intro {
                        selected: (*selected + 1) % data.sources.len(),
                    };
                }

                KeyCode::Enter => {
                    *self = State::LoadingGenerator(*selected);
                }
                _ => (),
            },

            State::Typing(type_box) => {
                match event.code {
                    KeyCode::Char(c) => type_box.input(c),
                    KeyCode::Backspace => type_box.backspace(),
                    _ => (),
                }

                if let Some(stats) = type_box.finish() {
                    *self = State::Stats(stats);
                }
            }

            State::Stats(_) => match event.code {
                KeyCode::Enter => *self = State::Intro { selected: 0 },
                _ => (),
            },

            State::LoadingGenerator { .. } => (),
        }
        Ok(())
    }
}

struct Global {
    sources: SourceLoader,
    rng: StdRng,
}

fn main() -> anyhow::Result<()> {
    let mut global = Global {
        sources: SourceLoader::new(),
        rng: StdRng::from_entropy(),
    };
    let mut state = State::Intro { selected: 0 };
    let mut terminal = Backend::new(BufWriter::new(stdout())).context("create backend")?;

    loop {
        state.update(&mut global).context("state update")?;
        state.draw(&mut terminal, &global).context("layout draw")?;
        if event::poll(Duration::from_millis(100)).context("event poll")? {
            match event::read().context("event read")? {
                Event::Key(KeyEvent {
                    code: KeyCode::Esc, ..
                }) => break,
                Event::Key(event) => state.handle_event(event, &mut global)?,
                Event::Resize(_, _) => (),
                Event::Mouse(_) => (),
            }
        }
    }

    terminal.finish()?;
    Ok(())
}

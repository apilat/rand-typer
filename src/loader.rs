use crate::markov::Chain;
use crate::word_source::{CommonWords, DummySource, WordSource};
use anyhow::Context;
use once_cell::sync::OnceCell;
use std::cell::Cell;
use std::fs::{self, File};
use std::io::{BufReader, BufWriter};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

struct Source {
    name: &'static str,
    // TODO Figure out a way to get rid of this Mutex, although it shouldn't impact performance
    // since it is only accessed in case of error.
    src: Arc<OnceCell<Result<Box<dyn WordSource + Send + Sync>, Mutex<Option<anyhow::Error>>>>>,
    gen: Cell<Option<fn() -> anyhow::Result<Box<dyn WordSource + Send + Sync>>>>,
}

impl Source {
    fn new(name: &'static str, src: Box<dyn WordSource + Send + Sync>) -> Self {
        Source {
            name,
            src: Arc::new(OnceCell::from(Ok(src))),
            gen: Cell::new(None),
        }
    }

    fn lazy(
        name: &'static str,
        gen: fn() -> anyhow::Result<Box<dyn WordSource + Send + Sync>>,
    ) -> Self {
        Source {
            name,
            src: Arc::new(OnceCell::new()),
            gen: Cell::new(Some(gen)),
        }
    }
}

pub struct SourceLoader {
    sources: Vec<Source>,
}

impl SourceLoader {
    pub fn new() -> Self {
        let mut sources = Vec::new();

        if cfg!(debug_assertions) {
            sources.push(Source::new("Dummy ready", Box::new(DummySource)));
            sources.push(Source::lazy("Dummy loadable", || {
                thread::sleep(Duration::from_secs(2));
                Ok(Box::new(DummySource))
            }));
        }

        sources.push(Source::new(
            "Random common words (top 1000)",
            Box::new(CommonWords),
        ));

        sources.push(Source::lazy("Markov chain (3-gram)", || {
            if let Ok(model) = File::open("res/chain.bin") {
                let chain = bincode::deserialize_from::<_, Chain<_, 3>>(BufReader::with_capacity(
                    1_048_576, model,
                ))
                .context("deserialization failed")?;
                return Ok(Box::new(chain));
            }

            let mut texts = Vec::new();
            for file in fs::read_dir("res/books/").context("read res/books/ directory")? {
                let file = file.context("read file inside res/books/")?.path();
                texts.push(
                    fs::read_to_string(&file)
                        .with_context(|| format!("read book {}", file.display()))?,
                );
            }
            let chain =
                Chain::<_, 3>::from_project_gutenberg(texts).context("markov chain creation")?;

            bincode::serialize_into(
                BufWriter::with_capacity(
                    1_048_576,
                    File::create("res/chain.bin").context("model write")?,
                ),
                &chain,
            )
            .context("serialization failed")?;

            Ok::<_, anyhow::Error>(Box::new(chain))
        }));

        SourceLoader { sources }
    }

    pub fn get(&self, id: usize) -> Option<anyhow::Result<&dyn WordSource>> {
        if let Some(src) = self.sources[id].src.get() {
            Some(match src {
                Ok(x) => Ok(&**x),
                Err(e) => {
                    Err(e.lock().unwrap().take().unwrap_or_else(|| {
                        anyhow::anyhow!("source creation error already collected")
                    }))
                }
            })
        } else {
            if let Some(gen) = self.sources[id].gen.take() {
                let dest = self.sources[id].src.clone();
                thread::spawn(move || {
                    if dest.set(gen().map_err(|e| Mutex::new(Some(e)))).is_err() {
                        panic!("invalid source creation race");
                    }
                });
            }
            None
        }
    }

    pub fn names(&self) -> impl Iterator<Item = &str> {
        self.sources.iter().map(|src| src.name)
    }

    pub fn len(&self) -> usize {
        self.sources.len()
    }
}

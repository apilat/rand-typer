use crate::ui::{DrawContext, Size, Tile, Widget};
use anyhow::anyhow;
use crossterm::style::{Attribute, Color};
use std::time::{Duration, Instant};

const WIDTH: usize = 80;
const HEIGHT: usize = 16;

#[derive(Debug, Clone)]
pub struct TypeBox {
    text: Vec<char>,
    typed: Vec<char>,
    text_str: String,
    start_time: Option<Instant>,

    correct: usize,
    incorrect: usize,
}

impl TypeBox {
    pub fn new(text: String) -> anyhow::Result<TypeBox> {
        if text.len() > WIDTH * HEIGHT {
            Err(anyhow!(
                "text length {} exceeds maximum {}",
                text.len(),
                WIDTH * HEIGHT
            ))
        } else {
            Ok(TypeBox {
                text: text.chars().collect(),
                typed: Vec::new(),
                text_str: text,
                start_time: None,

                correct: 0,
                incorrect: 0,
            })
        }
    }

    pub fn input(&mut self, c: char) {
        if self.start_time.is_none() {
            self.start_time = Some(Instant::now());
        }

        if self.typed.len() < self.text.len() {
            if c == self.text[self.typed.len()] {
                self.correct += 1;
            } else {
                self.incorrect += 1;
            }
            self.typed.push(c);
        }
    }

    pub fn backspace(&mut self) {
        // No-op if self.typed is empty
        self.typed.pop();
    }

    pub fn finish(&self) -> Option<Stats> {
        if self.text.len() == self.typed.len() {
            Some(Stats {
                accuracy: self.overall_accuracy(),
                wpm: self.adjusted_wpm(),
            })
        } else {
            None
        }
    }

    fn final_accuracy(&self) -> f32 {
        let (mut correct, mut total) = (0, 0);
        for (t, c) in self.typed.iter().zip(self.text.iter()) {
            total += 1;
            if t == c {
                correct += 1
            }
        }
        correct as f32 / total as f32
    }

    fn overall_accuracy(&self) -> f32 {
        self.correct as f32 / (self.correct + self.incorrect) as f32
    }

    fn wpm(&self) -> f32 {
        let time = self.start_time.map_or(Duration::ZERO, |i| i.elapsed());
        (self.text.len() as f32 / 5.0) / (time.as_secs_f32() / 60.0)
    }

    fn adjusted_wpm(&self) -> f32 {
        self.wpm() * self.final_accuracy()
    }
}

#[derive(Debug, Clone)]
pub struct Stats {
    pub accuracy: f32,
    pub wpm: f32,
}

impl Widget for &TypeBox {
    fn size(&self) -> (Size, Size) {
        (Size::Exact(WIDTH as u16), Size::Exact(HEIGHT as u16))
    }

    fn draw_to(&self, ctx: &mut DrawContext) -> anyhow::Result<()> {
        let mut finished_typed = false;

        // It is probably more correct to iterate over graphemes instead, but this is significantly
        // harder to handle. Might be implemented in the future.
        for (i, c) in self.text.iter().enumerate() {
            let mut tile = Tile::default().char(*c);

            match self.typed.get(i) {
                Some(t) => {
                    if t == c {
                        tile = tile.fg(Color::Green);
                    } else {
                        tile = tile.bg(Color::Red);
                    }
                }
                None => {
                    if !finished_typed {
                        finished_typed = true;
                        tile = tile.style(Attribute::Reverse);
                    }
                }
            };

            ctx.with_offset(((i % WIDTH) as u16, (i / WIDTH) as u16), |ctx| {
                ctx.print_tile(tile)
            })?;
        }

        Ok(())
    }
}
